<?php

namespace app\events;

use Yii;
use yii\base\Event;

/**
 *
 */
class NotificationEvent extends Event
{
    /**
     * @var yii\base\Model
     */
    public $model;

    /**
     * @var array
     */
    public $extraData = [];
}
