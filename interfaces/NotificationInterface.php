<?php

namespace app\interfaces;

/**
 *
 */
interface NotificationInterface
{
    /**
     *
     */
    public function getNotificationTitle();

    /**
     *
     */
    public function getNotificationContent();
}
