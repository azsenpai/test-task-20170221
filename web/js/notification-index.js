jQuery(function($) {

    $('.btn-send').on('click', function() {
        var $btn = $(this);

        if ($btn.hasClass('disabled')) {
            return;
        }

        var ids = [];

        $('[name="selection[]"]:checked').each(function() {
            ids.push($(this).val());
        });

        if (ids.length == 0) {
            alert('Выберите уведомления');
            return;
        }

        var roles = [];

        $('[name="role[]"]:checked').each(function() {
            roles.push($(this).val());
        });

        if (roles.length == 0) {
            alert('Выберите роли');
            return;
        }

        var notification_types = [];

        $('[name="notification_type[]"]:checked').each(function() {
            notification_types.push($(this).val());
        });

        if (notification_types.length == 0) {
            alert('Выберите типы уведомления');
            return;
        }

        $btn.addClass('disabled');

        $.ajax({
            url: '/notification/send',
            type: 'post',
            data: {
                ids: ids,
                roles: roles,
                notification_types: notification_types
            },
            success: function(response) {
                if (response === true) {
                    $.notify({
                        message: 'Уведомления успешно добавлены в очередь'
                    }, {
                        type: 'success',
                        offset: {
                            x: 20,
                            y: 60
                        },
                        newest_on_top: true
                    });

                    $('[name="selection[]"]:checked').attr('checked', false);

                    $('[name="role[]"]:checked').attr('checked', false);

                    $('[name="notification_type[]"]:checked').attr('checked', false);
                }

                $btn.removeClass('disabled');
            }
        });
    });

});