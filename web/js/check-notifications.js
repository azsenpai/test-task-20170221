jQuery(function($) {

    var genTimestamp;
    var used = {};

    getNotifications();

    function getNotifications() {
        $.ajax({
            url: '/notification/ajax-list',
            type: 'get',
            data: {
                genTimestamp: genTimestamp
            },
            success: function(response) {
                if (!('genTimestamp' in response) || !('result' in response)) {
                    return;
                }

                genTimestamp = response.genTimestamp;

                for (var i in response.result) {
                    notification = response.result[i];

                    if (notification.id in used) {
                        continue;
                    }

                    used[notification.id] = true;

                    $.notify({
                        icon: 'glyphicon glyphicon-info-sign',
                        title: notification.title + ':',
                        message: notification.content + '...'
                    }, {
                        type: 'info',
                        delay: 0,
                        offset: {
                            x: 20,
                            y: 60
                        },
                        onShow: function() {
                            $(this).data('notification_id', notification.id);
                        },
                        onClose: onNotificationClosed
                    });
                }

                setTimeout(getNotifications, 5000);
            },
            error: function() {
                setTimeout(getNotifications, 5000);
            }
        });
    }

    function onNotificationClosed() {
        var id = this.data('notification_id');

        $.ajax({
            url: '/notification/close',
            type: 'post',
            data: {
                id: id
            }
        });
    }
});
