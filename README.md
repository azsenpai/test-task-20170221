Для того что бы установить проект, выполните пожалуйста следующие команды:

~~~
git clone https://coder_iitu@bitbucket.org/coder_iitu/test-task-20170221.git
~~~

~~~
cd test-task-20170221
~~~

~~~
composer install
~~~

Название базы `test-task-20170221`, доступы должны быть у `root` без пароля

~~~
yii migrate --migrationPath=@yii/rbac/migrations
~~~

~~~
yii migrate
~~~

~~~
yii rbac/init
~~~

~~~
yii user/init
~~~

В проекте можно было использовать `cron`, `redis`, `beanstalkd`  
Для того что бы добавить новый тип уведомления нужно добавить константу в `app\models\Notification`, изменить метод `types`, и добавить метод в `commands\NotificationController` с названием `actionProccess{новый тип}`