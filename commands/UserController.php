<?php

namespace app\commands;

use yii\console\Controller;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use app\models\User;

/**
 *
 */
class UserController extends Controller
{
    /**
     *
     */
    public function actionInit()
    {
        $admin = new User();

        $admin->username = 'admin';
        $admin->setPassword('admin');
        $admin->email = 'zh.adlet@gmail.com';
        $admin->status = User::STATUS_ACTIVE;

        if ($admin->save()) {
            $admin->setNotifications();

            $this->stdout('User "admin" is created.' . PHP_EOL);
        } else {
            VarDumper::dump($admin->errors);
        }

        $moderator = new User();

        $moderator->username = 'moderator';
        $moderator->setPassword('moderator');
        $moderator->email = 'test-task-20170221@yandex.ru';
        $moderator->status = User::STATUS_ACTIVE;

        if ($moderator->save()) {
            $moderator->setNotifications();

            $this->stdout('User "moderator" is created.' . PHP_EOL);
        } else {
            VarDumper::dump($moderator->errors);
        }
    }

    /**
     *
     */
    public function actionGetActivateLink($email)
    {
        $user = User::findByEmail($email);

        if ($user) {
            $this->stdout(Url::to(['user/activate', 'email' => $user->email, 'token' => $user->activate_token], true) . PHP_EOL);
        } else {
            $this->stdout("User not found by email '$email'");
        }
    }
}
