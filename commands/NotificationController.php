<?php

namespace app\commands;

use Yii;
use yii\helpers\ArrayHelper;
use yii\console\Controller;
use app\models\Notification;
use app\models\NotificationStatus;

/**
 *
 */
class NotificationController extends Controller
{
    /**
     *
     */
    public function actionProcessEmail()
    {
        $cache = Yii::$app->cache;

        $key = sprintf('%s_%s', $this->id, $this->action->id);
        $value = $cache->get($key);

        if ($value !== false) {
            return;
        }

        $cache->set($key, true, 15 * 60); // 15min

        $models = NotificationStatus::find()
            ->where([
                'notification_type' => Notification::TYPE_EMAIL,
                'status' => Notification::STATUS_NOT_VIEWED,
            ])
            ->all();

        $messages = [];

        foreach ($models as $model) {
            $notification = $model->notification->model;

            $messages[] = Yii::$app->mailer
                ->compose(
                    ['html' => 'notification-html'],
                    [
                        'title' => $notification->notificationTitle,
                        'content' => $notification->notificationContent,
                    ]
                )
                ->setTo($model->user->email)
                ->setSubject('У вас новое уведомление');
        }

        if (!empty($messages)) {
            Yii::$app->mailer->sendMultiple($messages);

            NotificationStatus::updateAll(
                ['status' => Notification::STATUS_PROCESSED],
                ['id' => ArrayHelper::getColumn($models, 'id')]
            );
        }

        $cache->delete($key);
    }

    /**
     *
     */
    public function actionProcessBrowser()
    {
    }
}
