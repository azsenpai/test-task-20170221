<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Успешная регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-registration-success">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-success">
        <p>Ваша регистрация успешно завершена.</p>
        <p>Мы отправили письмо с подтверждением на Ваш электронный ящик.</p>
    </div>
</div>
