<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Главная';
?>
<div class="site-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if (Yii::$app->user->can('createPost')): ?>
    <p>
        <?= Html::a('Создать новость', ['news/create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => Yii::$app->user->can('author') ? $searchModel : null,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'visible' => Yii::$app->user->can('author'),
            ],
            'title',
            'preview_text:ntext',
            // 'full_text:ntext',
            'created_at:datetime',
            // 'updated_at',
            [
                'attribute' => 'created_by',
                'value' => function($model) {
                    return ArrayHelper::getValue($model, 'createdBy.username');
                },
            ],
            // 'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => 'news',
                'visibleButtons' => [
                    /*'view' => function ($model, $key, $index) {
                        return Yii::$app->user->can('readPost');
                    },*/
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->can('updatePost', ['post' => $model]);
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->can('deletePost', ['post' => $model]);
                    },
                ],
                'options' => ['width' => 80],
            ],
        ],
    ]); ?>
</div>
