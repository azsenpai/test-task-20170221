<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;
use app\models\Notification;

/* @var $this yii\web\View */

$this->registerJsFile('js/notification-index.js', [
    'depends' => [
        'yii\web\JqueryAsset',
    ],
]);

$this->title = 'Уведомления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    return ['value' => $model->id];
                },
            ],

            // 'id',

            [
                'label' => 'Название',
                'attribute' => 'model.notificationTitle',
            ],

            [
                'label' => 'Содержание',
                'attribute' => 'model.notificationContent',
            ],
        ],
    ]); ?>

    <div class="well">
        <div class="row">
            <div class="col-lg-3">
                <label class="control-label">Роли:</label>
                <?php foreach (User::roles() as $type => $label): ?>
                    <div class="form-group">
                        <?= Html::checkbox('role[]', false, ['label' => $type, 'value' => $type]) ?>
                    </div>
                <?php endforeach ?>
            </div>
            <div class="col-lg-3">
                <label class="control-label">Типы уведомления:</label>
                <?php foreach (Notification::types() as $type => $label): ?>
                    <div class="form-group">
                        <?= Html::checkbox('notification_type[]', false, ['label' => $type, 'value' => $type]) ?>
                    </div>
                <?php endforeach ?>
            </div>
        </div>

        <span class="btn btn-primary btn-send">Отправить</span>
    </div>

</div>
