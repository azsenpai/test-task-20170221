<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'username',
                'value' => function($model) {
                    $value = $model->username;
                    $role = $model->roleName;

                    if (!empty($role)) {
                        $value .= " ($role)";
                    }

                    return $value;
                },
            ],
            /*
            [
                'class' => 'yii\grid\Column',
                'header' => 'Роль',
                'content' => function ($model, $key, $index, $column) {
                    return $model->roleName;
                },

            ],
            */
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            // 'activate_token',
            'email:email',
            [
                'attribute' => 'status',
                'value' => function($model) {
                    return $model->statusName;
                },
                'filter' => User::statuses(),
            ],
            // 'notification_email',
            // 'notification_browser',
            'created_at:datetime',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
