<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Notification;

$this->title = 'Настройки аккаунта';

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if ($success): ?>
        <div class="alert alert-success">
            <p>Ваши настройки успешно сохранены.</p>
        </div>
    <?php endif ?>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-lock"></span> Сменить пароль', ['change-password'], ['class' => 'btn btn-sm btn-warning']) ?>
    </p>

    <?php $form = ActiveForm::begin(); ?>

        <?php foreach (Notification::types() as $type => $label): ?>
            <div class="form-group">
                <?= Html::checkbox(sprintf('Notification[%s]', $type), isset($notifications[$type]), ['label' => $label]) ?>
            </div>
        <?php endforeach ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
