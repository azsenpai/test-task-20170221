<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Успешная активация';

$this->params['breadcrumbs'][] = ['label' => Yii::$app->user->identity->username, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-activate-success">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-success">
        <p>Вы успешно активировали свой аккаунт!</p>
    </div>
</div>
