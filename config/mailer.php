<?php

return [
    'class' => 'yii\swiftmailer\Mailer',
    // send all mails to a file by default. You have to set
    // 'useFileTransport' to false and configure a transport
    // for the mailer to send real emails.
    'useFileTransport' => false,

    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'smtp.yandex.ru',
        'username' => 'test-task-20170221@yandex.ru',
        'password' => 'Asdk82!_Qwq4',
        'port' => '465',
        'encryption' => 'ssl',
    ],

    'messageConfig' => [
        'from' => $params['infoEmail'],
    ],
];
