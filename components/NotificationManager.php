<?php

namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\Event;
use app\behaviors\UserBehavior;
use app\behaviors\NewsBehavior;
use app\behaviors\NotificationBehavior;

/**
 *
 */
class NotificationManager extends Component
{
    const EVENT_USER_CREATED = 'userCreated';
    const EVENT_USER_CREATED_BY_ADMIN = 'userCreatedByAdmin';
    const EVENT_USER_PASSWORD_CHANGED = 'userPasswordChanged';

    const EVENT_NEWS_CREATED = 'newsCreated';

    const EVENT_NOTIFICATION_CREATED = 'notificationCreated';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            UserBehavior::className(),
            NewsBehavior::className(),
            NotificationBehavior::className(),
        ];
    }
}
