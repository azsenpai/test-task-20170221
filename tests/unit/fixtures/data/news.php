<?php

return [
    [
        'title' => 'Quia magni incidunt et sit.',
        'preview_text' => 'Excepturi quia ut dolorem accusamus. Officiis eligendi ipsa enim voluptas iure quod. Libero non quia et est.',
        'full_text' => 'Impedit eveniet tenetur fugit. Sit eos et sunt dolorum perspiciatis. Explicabo voluptas ut aut optio dolorem assumenda. Dolores accusamus autem autem ipsum earum expedita. Est sed eum sit nobis harum enim sint. Quos eveniet aut quis sit enim hic sint et. Laborum voluptatem molestiae enim ad impedit ullam et. Et odio est praesentium exercitationem at labore perspiciatis. Iusto facere quisquam voluptate veniam nostrum sint. Modi eum culpa eius provident labore dolorum.',
    ],
    [
        'title' => 'Commodi molestiae error debitis eaque.',
        'preview_text' => 'Omnis non voluptatem error magnam est est praesentium quia. Id eveniet repudiandae soluta et est cumque. Mollitia dolore aliquid tempora.',
        'full_text' => 'Nihil dolores rerum et sit esse. Magnam delectus omnis eligendi quae. Sed molestiae dolorum perspiciatis beatae in id. Omnis non porro ipsam fuga sed. Sed sapiente voluptates eligendi quo placeat magni rem et. Ipsum consequuntur aut voluptas id ratione est autem. Sit cumque porro consequatur dolorem dolores aut. Vero quos unde ipsam sunt at. Nihil at odit mollitia iste hic laborum est in. Tempora repudiandae voluptas aliquid quasi non exercitationem.',
    ],
    [
        'title' => 'Autem enim animi odio error consequatur quod.',
        'preview_text' => 'Repellat repudiandae omnis ad. Enim impedit facilis consectetur. Atque quia excepturi a blanditiis molestiae libero libero.',
        'full_text' => 'Sunt natus veniam blanditiis dolores officia incidunt laudantium. Consequatur sapiente praesentium rerum et quia. Sint nihil molestias quia pariatur aspernatur quod. Alias veniam est nulla vero soluta. Cupiditate dolores minus debitis quis voluptates consectetur similique dolorum. Voluptas cumque deleniti nesciunt. Deleniti et accusamus libero. Unde non inventore aut sunt nihil tenetur. Iure maiores est quasi aut. Et non magnam quo tempora dolor sit molestias dignissimos.',
    ],
];
