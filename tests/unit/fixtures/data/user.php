<?php

return [
    [
        'username' => 'gnitzsche',
        'auth_key' => 'ljbE4buPdtFu6KtRwbtoM5qayzPt2iRV',
        'password' => '$2y$13$zr1fjKucGH3Bb3SzI2PeruJ.wa/njWanf8n3MAu9v60Pdo8F8fw5e',
        'password_reset_token' => '',
        'activate_token' => 'B13pehCYTOZvPD1v75_4WtZipAR4SMNO',
        'email' => 'evan.stracke@mayer.net',
    ],
    [
        'username' => 'abe52',
        'auth_key' => 'GkaFa_76BR9dwJzmSlHOqkfiCUqFmFyX',
        'password' => '$2y$13$LbVdoxQtYzRSzfF0LkVSAe36KNEf.xwdfv6aDu8FodhW152rChOwm',
        'password_reset_token' => '',
        'activate_token' => 'XWU34hvjDG5BI8MMponuVYCutU65xB5q',
        'email' => 'hester42@mohr.org',
    ],
];
