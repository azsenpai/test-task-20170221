<?php

return [
    'title' => $faker->sentence(),
    'preview_text' => $faker->sentences(3, true),
    'full_text' => $faker->sentences(10, true),
];
