<?php

return [
    'username' => $faker->username,
    'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
    'password' => Yii::$app->getSecurity()->generatePasswordHash('password_' . $index),
    'password_reset_token' => '',
    'activate_token' => Yii::$app->getSecurity()->generateRandomString(),
    'email' => $faker->email,
];
