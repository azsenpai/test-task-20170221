<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use app\models\User;
use app\models\AccountForm;
use app\models\ChangePasswordForm;

/**
 *
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'activate-success', 'change-password'],
                'rules' => [
                    [
                        'actions' => ['index', 'activate-success', 'change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;

        if (Yii::$app->request->isPost) {
            $notifications = [];

            foreach (Yii::$app->request->post('Notification', []) as $type => $value) {
                if ($value == 1) {
                    $notifications[] = $type;
                }
            }

            $user->notifications = $notifications;

            return $this->redirect(['index', 'success' => 1]);
        }

        $notifications = $user->notifications;
        $notifications = ArrayHelper::map($notifications, 'notification_type', 'user_id');

        return $this->render('index', [
            'success' => Yii::$app->request->get('success', 0),
            'notifications' => $notifications,
        ]);
    }

    /**
     *
     */
    public function actionActivate($email, $token)
    {
        $user = User::findByEmail($email);

        if ($user && $user->status == User::STATUS_NOT_VERIFIED
            && $user->validateActivateToken($token)
        ) {
            $user->updateAttributes(['status' => User::STATUS_ACTIVE]);

            Yii::$app->user->login($user);

            return $this->redirect(['activate-success']);
        }

        return $this->goHome();
    }

    /**
     *
     */
    public function actionActivateSuccess()
    {
        return $this->render('activate-success');
    }

    /**
     *
     */
    public function actionChangePassword()
    {
        $model = new ChangePasswordForm(Yii::$app->user->identity);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['change-password', 'success' => 1]);
        }

        return $this->render('change-password', [
            'success' => Yii::$app->request->get('success', 0),
            'model' => $model,
        ]);
    }
}
