<?php

namespace app\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\data\ActiveDataProvider;
use app\models\Notification;
use app\models\NotificationStatus;
use app\models\User;

/**
 *
 */
class NotificationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['ajax-list', 'close'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Notification::find(),
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *
     */
    public function actionAjaxList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cache = Yii::$app->cache;

        $resultKey = sprintf('result-%d', Yii::$app->user->id);
        $result = $cache->get($resultKey);

        if ($result === false) {
            $result = [];
        }

        $genTimestampKey = sprintf('genTimestamp-%d', Yii::$app->user->id);
        $genTimestamp = $cache->get($genTimestampKey);

        $notificationUpdatedAt = $cache->get('notificationUpdatedAt');

        if ($genTimestamp === false || $genTimestamp < $notificationUpdatedAt) {
            $lastId = 0;

            foreach ($result as $item) {
                $lastId = max($lastId, $item['id']);
            }

            $models = NotificationStatus::find()
                ->where([
                    'user_id' => Yii::$app->user->id,
                    'notification_type' => Notification::TYPE_BROWSER,
                    'status' => Notification::STATUS_NOT_VIEWED,
                ])
                ->andWhere(['>', 'id', $lastId])
                ->all();

            foreach ($models as $model) {
                $notification = $model->notification;

                $result[$model->id] = [
                    'id' => $model->id,
                    'title' => $notification->model->notificationTitle,
                    'content' => $notification->model->notificationContent,
                ];
            }

            $genTimestamp = empty($notificationUpdatedAt)
                ? time()
                : $notificationUpdatedAt;
                
            $cache->mset([
                $genTimestampKey => $genTimestamp,
                $resultKey => $result,
            ]);
        }

        $clientGenTimestamp = (int)Yii::$app->request->get('genTimestamp');

        if ($genTimestamp == $clientGenTimestamp) {
            $result = [];
        }

        return [
            'genTimestamp' => $genTimestamp,
            'result' => $result,
        ];
    }

    /**
     *
     */
    public function actionClose()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cache = Yii::$app->cache;

        $key = sprintf('result-%d', Yii::$app->user->id);
        $result = $cache->get($key);

        if ($result === false) {
            $result = [];
        }

        $id = Yii::$app->request->post('id');

        if ($id === null) {
            throw new InvalidParamException('id must be set');
        }

        if (isset($result[$id])) {
            unset($result[$id]);
            $cache->set($key, $result);
        }

        NotificationStatus::updateAll(
            ['status' => Notification::STATUS_PROCESSED],
            ['id' => $id]
        );

        return true;
    }

    /**
     *
     */
    public function actionSend()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $ids = Yii::$app->request->post('ids', []);
        $roles = Yii::$app->request->post('roles', []);
        $notificationTypes = Yii::$app->request->post('notification_types', []);

        $userIds = [];

        foreach ($roles as $role) {
            $userIds = array_merge($userIds, Yii::$app->authManager->getUserIdsByRole($role));
        }

        $columns = ['notification_id', 'user_id', 'notification_type', 'status', 'created_at', 'updated_at'];
        $rows = [];

        $time = time();

        foreach ($ids as $id) {
            foreach ($notificationTypes as $notificationType) {
                foreach ($userIds as $userId) {
                    $rows[] = [
                        'notification_id' => $id,
                        'user_id' => $userId,
                        'notification_type' => $notificationType,
                        'status' => Notification::STATUS_NOT_VIEWED,
                        'created_at' => $time,
                        'updated_at' => $time,
                    ];
                }
            }
        }

        if (count($rows) > 0) {
            Yii::$app->db->createCommand()
                ->batchInsert(NotificationStatus::tableName(), $columns, $rows)
                ->execute();

            Yii::$app->cache->set('notificationUpdatedAt', $time);

            Notification::processAll();
        }

        return true;
    }
}
