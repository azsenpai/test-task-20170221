<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\NotificationManager;
use app\events\NotificationEvent;
use app\helpers\ConsoleRunner;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property string $model_class_name
 * @property integer $model_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property NotificationStatus[] $notificationStatuses
 */
class Notification extends \yii\db\ActiveRecord
{
    const TYPE_EMAIL = 'email';
    const TYPE_BROWSER = 'browser';

    const STATUS_NOT_VIEWED = 0;
    const STATUS_PROCESSED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     *
     */
    public static function types()
    {
        return [
            self::TYPE_EMAIL => 'Получать уведомления на почту',
            self::TYPE_BROWSER => 'Получать уведомления в браузере',
        ];
    }

    /**
     *
     */
    public static function processAll()
    {
        foreach (self::types() as $type => $label) {
            ConsoleRunner::exec(sprintf('notification/process-%s', $type));
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $event = new NotificationEvent([
                'model' => $this,
            ]);

            Yii::$app->notificationManager->trigger(NotificationManager::EVENT_NOTIFICATION_CREATED, $event);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_class_name', 'model_id'], 'required'],
            [['model_id', 'created_at', 'updated_at'], 'integer'],
            [['model_class_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_class_name' => 'Model Class Name',
            'model_id' => 'Model ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationStatuses()
    {
        return $this->hasMany(NotificationStatus::className(), ['notification_id' => 'id']);
    }

    /**
     *
     */
    public function getModel()
    {
        return $this->hasOne($this->model_class_name, ['id' => 'model_id']);
    }
}
