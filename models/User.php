<?php

namespace app\models;

use Yii;

use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\components\NotificationManager;
use app\events\NotificationEvent;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $activate_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_NOT_VERIFIED = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_LOCKED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     *
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }

    /**
     *
     */
    public static function statuses()
    {
        return [
            self::STATUS_NOT_VERIFIED => 'Не подтвержден',
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_LOCKED => 'Заблокирован',
        ];
    }

    /**
     *
     */
    public static function roles()
    {
        $roles = Yii::$app->authManager->roles;

        return ArrayHelper::map($roles, 'name', 'name');
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->generateAuthKey();
                $this->generateActivateToken();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $event = new NotificationEvent([
                'model' => $this,
            ]);

            Yii::$app->notificationManager->trigger(NotificationManager::EVENT_USER_CREATED, $event);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_NOT_VERIFIED],

            // [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],

            [['username', 'password_hash', 'email'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'activate_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'E-mail',
            'status' => 'Статус',
            'created_at' => 'Создан',
            'updated_at' => 'Отредактирован',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     *
     */
    public function generateActivateToken()
    {
        $this->activate_token = Yii::$app->security->generateRandomString();
    }

    /**
     *
     */
    public function validateActivateToken($token)
    {
        return $this->activate_token === $token;
    }

    /**
     *
     */
    public function removeActivateToken()
    {
        $this->activate_token = null;
    }

    /**
     *
     */
    public function getStatusName()
    {
        $statuses = self::statuses();
   
        return ArrayHelper::getValue($statuses, $this->status);
    }

    /**
     *
     */
    public function getRoles()
    {
        return Yii::$app->authManager->getRolesByUser($this->id);
    }

    /**
     *
     */
    public function getRoleName()
    {
        $roles = $this->getRoles();

        if (!empty($roles)) {
            return reset($roles)->name;
        }

        return null;
    }

    /**
     *
     */
    public function setRole($roleName)
    {
        $auth = Yii::$app->authManager;

        $auth->revokeAll($this->id);
        $role = $auth->getRole($roleName);

        if ($role) {
            $auth->assign($role, $this->id);
        }
    }

    /**
     *
     */
    public function getNotifications()
    {
        return $this->hasMany(UserNotification::className(), ['user_id' => 'id']);
    }

    /**
     *
     */
    public function setNotifications($notifications = null)
    {
        $this->clearNotifications();

        if ($notifications === null) {
            $notifications = array_keys(Notification::types());
        }

        $columns = ['user_id', 'notification_type'];
        $rows = [];

        foreach ($notifications as $notification_type) {
            $rows[] = [
                $this->id,
                $notification_type,
            ];
        }

        if (!empty($rows)) {
            Yii::$app->db->createCommand()
                ->batchInsert(UserNotification::tableName(), $columns, $rows)
                ->execute();
        }
    }

    /**
     *
     */
    public function clearNotifications()
    {
        UserNotification::deleteAll(['user_id' => $this->id]);
    }

    /**
     *
     */
    public function hasNotification($notification_type)
    {
        $notifications = ArrayHelper::getColumn($this->notifications, 'notification_type');

        return in_array($notification_type, $notifications);
    }
}
