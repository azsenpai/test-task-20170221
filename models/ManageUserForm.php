<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Model;
use app\components\NotificationManager;
use app\events\NotificationEvent;

/**
 *
 */
class ManageUserForm extends Model
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $username;
    public $password;
    public $email;
    public $role;
    public $status;

    /**
     * @var User
     */
    private $_user;

    /**
     *
     */
    public function __construct(User $user = null, $config = [])
    {
        $this->setUser($user);

        parent::__construct($config);
    }

    /**
     *
     */
    public function init()
    {
        $user = $this->getUser();

        Yii::configure($this, [
            'username' => $user->username,
            'email' => $user->email,
            'role' => $user->roleName,
            'status' => $user->status,
        ]);
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'on' => self::SCENARIO_CREATE],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'on' => self::SCENARIO_CREATE],

            ['password', 'required', 'on' => self::SCENARIO_CREATE],
            ['password', 'string'],

            [['status'], 'integer'],

            ['role', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => $this->scenario == self::SCENARIO_CREATE ? 'Пароль' : 'Новый пароль',
            'email' => 'E-mail',
            'role' => 'Роль',
            'status' => 'Статус',
        ];
    }

    /**
     * @return boolean
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser();

        Yii::configure($user, [
            'username' => $this->username,
            'email' => $this->email,
            'status' => $this->status,
        ]);

        if (!empty($this->password)) {
            $user->setPassword($this->password);
        }

        if ($user->save()) {
            $user->role = $this->role;

            $user->setNotifications();

            if ($this->scenario == self::SCENARIO_CREATE) {
                $event = new NotificationEvent([
                    'model' => $user,
                    'extraData' => [
                        'password' => $this->password,
                    ],
                ]);

                Yii::$app->notificationManager->trigger(NotificationManager::EVENT_USER_CREATED_BY_ADMIN, $event);
            } else if (!empty($this->password)) {
                $event = new NotificationEvent([
                    'model' => $user,
                    'extraData' => [
                        'password' => $this->password,
                    ],
                ]);

                Yii::$app->notificationManager->trigger(NotificationManager::EVENT_USER_PASSWORD_CHANGED, $event);
            }

            return true;
        }

        return false;
    }

    /**
     *
     */
    public function getIsNewRecord()
    {
        return isset($this->_user) ? $this->_user->isNewRecord : false;
    }

    /**
     *
     */
    public function getId()
    {
        return isset($this->_user) ? $this->_user->id : null;
    }

    /**
     *
     */
    public function getUser()
    {
        if (!isset($this->_user)) {
            $this->_user = new User();
        }

        return $this->_user;
    }

    /**
     *
     */
    public function setUser($user)
    {
        $this->_user = $user;
    }
}
