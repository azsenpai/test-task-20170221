<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\NotificationManager;
use app\events\NotificationEvent;
use app\interfaces\NotificationInterface;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $preview_text
 * @property string $full_text
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class News extends \yii\db\ActiveRecord implements NotificationInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $event = new NotificationEvent([
                'model' => $this,
            ]);

            Yii::$app->notificationManager->trigger(NotificationManager::EVENT_NEWS_CREATED, $event);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'preview_text', 'full_text'], 'required'],
            [['preview_text', 'full_text'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'preview_text' => 'Превью',
            'full_text' => 'Полное описание',
            'created_at' => 'Создан',
            'updated_at' => 'Отредактирован',
            'created_by' => 'Создал',
            'updated_by' => 'Отредактировал',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     *
     */
    public function getNotificationTitle()
    {
        return 'У вас новое уведомление';
    }

    /**
     *
     */
    public function getNotificationContent()
    {
        $words = explode(' ', $this->title);

        return implode(' ', array_slice($words, 0, 4));
    }
}
