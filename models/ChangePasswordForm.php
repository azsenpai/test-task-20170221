<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\NotificationManager;
use app\events\NotificationEvent;

/**
 *
 */
class ChangePasswordForm extends Model
{
    public $password;
    public $new_password;

    /**
     * @var app\models\User
     */
    private $_user;

    /**
     *
     */
    public function __construct(User $user, $config = [])
    {
        $this->setUser($user);

        parent::__construct($config);
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['password', 'new_password'], 'required'],
            ['password', 'validatePassword']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Старый пароль',
            'new_password' => 'Новый пароль',
        ];
    }

    /**
     * @return boolean
     */
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }

        $user = $this->getUser();

        $user->setPassword($this->new_password);

        if ($user->save()) {
            $event = new NotificationEvent([
                'model' => $user,
                'extraData' => [
                    'password' => $this->new_password,
                ],
            ]);

            Yii::$app->notificationManager->trigger(NotificationManager::EVENT_USER_PASSWORD_CHANGED, $event);

            return true;
        }

        return false;
    }

    /**
     *
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, "Неправильный «Старый пароль».");
            }
        }
    }

    /**
     *
     */
    public function setUser(User $user)
    {
        $this->_user = $user;
    }

    /**
     *
     */
    public function getUser()
    {
        return $this->_user;
    }
}

