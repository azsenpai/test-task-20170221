<?php

namespace app\behaviors;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use app\components\NotificationManager;
use app\models\NotificationStatus;
use app\models\Notification;
use app\models\User;

/**
 *
 */
class NotificationBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            NotificationManager::EVENT_NOTIFICATION_CREATED => 'onNotificationCreated',
        ];
    }

    /**
     *
     */
    public function onNotificationCreated($event)
    {
        $model = $event->model;

        if (!($model instanceof Notification)) {
            throw new InvalidParamException('model must be instance of \app\models\Notification');
        }

        $columns = ['notification_id', 'user_id', 'notification_type', 'status', 'created_at', 'updated_at'];
        $rows = [];

        $users = User::find()
            ->where(['status' => User::STATUS_ACTIVE])
            ->all();

        $time = time();

        foreach ($users as $user) {
            $item = [
                'notification_id' => $model->id,
                'user_id' => $user->id,
                'notification_type' => null,
                'status' => Notification::STATUS_NOT_VIEWED,
                'created_at' => $time,
                'updated_at' => $time,
            ];

            foreach ($user->notifications as $notification) {
                $item['notification_type'] = $notification->notification_type;
                $rows[] = $item;
            }
        }

        if (count($rows) > 0) {
            Yii::$app->db->createCommand()
                ->batchInsert(NotificationStatus::tableName(), $columns, $rows)
                ->execute();

            Yii::$app->cache->set('notificationUpdatedAt', $time);

            Notification::processAll();
        }
    }
}
