<?php

namespace app\behaviors;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use app\components\NotificationManager;
use app\helpers\MailHelper;
use app\models\News;
use app\models\Notification;
use app\interfaces\NotificationInterface;

/**
 *
 */
class NewsBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            NotificationManager::EVENT_NEWS_CREATED => 'onNewsCreated',
        ];
    }

    /**
     *
     */
    public function onNewsCreated($event)
    {
        $model = $event->model;

        if (!($model instanceof NotificationInterface)) {
            throw new InvalidParamException('model must be instance of \app\interfaces\NotificationInterface');
        }

        $notification = new Notification([
            'model_class_name' => News::className(),
            'model_id' => $model->id,
        ]);

        $notification->save();
    }
}
