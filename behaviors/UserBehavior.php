<?php

namespace app\behaviors;

use Yii;
use yii\base\InvalidParamException;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use app\components\NotificationManager;
use app\helpers\MailHelper;
use app\models\User;

/**
 *
 */
class UserBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            NotificationManager::EVENT_USER_CREATED => 'onUserCreated',
            NotificationManager::EVENT_USER_CREATED_BY_ADMIN => 'onUserCreatedByAdmin',
            NotificationManager::EVENT_USER_PASSWORD_CHANGED => 'onUserPasswordChanged',
        ];
    }

    /**
     *
     */
    public function onUserCreated($event)
    {
        $model = $event->model;

        if (!($model instanceof User)) {
            throw new InvalidParamException('model must be instance of \app\models\User');
        }

        if ($model->status == User::STATUS_NOT_VERIFIED) {
            MailHelper::sendActivateLink($model);
        }

        MailHelper::notifyUserRegistration($model);
    }

    /**
     *
     */
    public function onUserCreatedByAdmin($event)
    {
        $model = $event->model;

        if (!($model instanceof User)) {
            throw new InvalidParamException('model must be instance of \app\models\User');
        }

        MailHelper::sendUserData($model, [
            'password' => ArrayHelper::getValue($event->extraData, 'password'),
        ]);
    }

    /**
     *
     */
    public function onUserPasswordChanged($event)
    {
        $model = $event->model;

        if (!($model instanceof User)) {
            throw new InvalidParamException('model must be instance of \app\models\User');
        }

        MailHelper::sendUserChangedPassword($model, [
            'password' => ArrayHelper::getValue($event->extraData, 'password'),
        ]);
    }
}
