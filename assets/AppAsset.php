<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use Yii;
use yii\web\AssetBundle;
use app\models\Notification;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\BootstrapNotifyAsset',
    ];

    /**
     *
     */
    public function init()
    {
        parent::init();

        if (!Yii::$app->user->isGuest) {
            $user = Yii::$app->user->identity;

            if ($user->hasNotification(Notification::TYPE_BROWSER)) {
                $this->js[] = 'js/check-notifications.js';
            }
        }
    }
}
