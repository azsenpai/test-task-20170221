<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 *
 */
class BootstrapNotifyAsset extends AssetBundle
{
    public $sourcePath = '@bower/remarkable-bootstrap-notify';

    public $css = [];

    public $js = [
        YII_DEBUG
            ? 'dist/bootstrap-notify.js'
            : 'dist/bootstrap-notify.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'delocker\animate\AnimateAssetBundle',
    ];
}
