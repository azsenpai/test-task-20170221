<?php

use yii\db\Migration;

class m170227_093908_user_notification extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_notification}}', [
            'user_id' => $this->integer()->notNull(),
            'notification_type' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'unique-user_notification-user_id-notification_type',
            'user_notification',
            [
                'user_id',
                'notification_type'
            ],
            true
        );

        // creates index for column `notification_id`
        $this->createIndex(
            'idx-user_notification-user_id',
            'user_notification',
            'user_id'
        );

        // add foreign key for table `user_notification`
        $this->addForeignKey(
            'fk-user_notification-user_id',
            'user_notification',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        // drops foreign key for table `user_notification`
        $this->dropForeignKey(
            'fk-user_notification-user_id',
            'user_notification'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_notification-user_id',
            'user_notification'
        );

        $this->dropIndex(
            'unique-user_notification-user_id-notification_type',
            'user_notification'
        );

        $this->dropTable('{{%user_notification}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
