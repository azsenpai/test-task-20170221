<?php

use yii\db\Migration;

class m170226_083412_notification_status extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notification_status}}', [
            'id' => $this->primaryKey(),

            'notification_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),

            'notification_type' => $this->string()->notNull(),
            'status' => $this->integer()->notNull(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        // creates index for column `notification_id`
        $this->createIndex(
            'idx-notification_status-notification_id',
            'notification_status',
            'notification_id'
        );

        // add foreign key for table `notification_status`
        $this->addForeignKey(
            'fk-notification_status-notification_id',
            'notification_status',
            'notification_id',
            'notification',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-notification_status-user_id',
            'notification_status',
            'user_id'
        );

        // add foreign key for table `notification_status`
        $this->addForeignKey(
            'fk-notification_status-user_id',
            'notification_status',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        // drops foreign key for table `notification_status`
        $this->dropForeignKey(
            'fk-notification_status-notification_id',
            'notification_status'
        );

        // drops index for column `notification_id`
        $this->dropIndex(
            'idx-notification_status-notification_id',
            'notification_status'
        );

        // drops foreign key for table `notification_status`
        $this->dropForeignKey(
            'fk-notification_status-user_id',
            'notification_status'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'fk-notification_status-user_id',
            'notification_status'
        );

        $this->dropTable('{{%notification_status}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
