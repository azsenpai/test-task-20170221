<?php

use yii\db\Migration;

class m170226_080851_notification extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notification}}', [
            'id' => $this->primaryKey(),

            'model_class_name' => $this->string()->notNull(),
            'model_id' => $this->integer()->notNull(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        // creates index for column `model_id`
        $this->createIndex(
            'idx-notification-model_id',
            'notification',
            'model_id'
        );
    }

    public function down()
    {
        // drops index for column `model_id`
        $this->dropIndex(
            'idx-notification-model_id',
            'notification'
        );

        $this->dropTable('{{%notification}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
