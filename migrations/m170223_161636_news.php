<?php

use yii\db\Migration;

class m170223_161636_news extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),

            'title' => $this->string()->notNull(),

            'preview_text' => $this->text()->notNull(),
            'full_text' => $this->text()->notNull(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        // creates index for column `created_by`
        $this->createIndex(
            'idx-news-created_by',
            'news',
            'created_by'
        );

        // add foreign key for table `news`
        $this->addForeignKey(
            'fk-news-created_by',
            'news',
            'created_by',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // creates index for column `updated_by`
        $this->createIndex(
            'idx-news-updated_by',
            'news',
            'updated_by'
        );

        // add foreign key for table `news`
        $this->addForeignKey(
            'fk-news-updated_by',
            'news',
            'updated_by',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        // drops foreign key for table `news`
        $this->dropForeignKey(
            'fk-news-created_by',
            'news'
        );

        // drops index for column `created_by`
        $this->dropIndex(
            'idx-news-created_by',
            'news'
        );

        // drops foreign key for table `news`
        $this->dropForeignKey(
            'fk-news-updated_by',
            'news'
        );

        // drops index for column `updated_by`
        $this->dropIndex(
            'idx-news-updated_by',
            'news'
        );

        $this->dropTable('{{%news}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
