<?php

namespace app\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\User;

/**
 *
 */
class MailHelper
{
    /**
     *
     */
    public static function send(array $data)
    {
        $view = ArrayHelper::getValue($data, 'view');
        $params = ArrayHelper::getValue($data, 'params');
        $from = ArrayHelper::getValue($data, 'from');
        $to = ArrayHelper::getValue($data, 'to');
        $subject = ArrayHelper::getValue($data, 'subject');

        return Yii::$app->mailer->compose($view, $params)
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->send();
    }

    /**
     * @param User $user
     */
    public static function sendActivateLink(User $user)
    {
        return static::send([
            'view' => ['html' => 'activate-link-html'],
            'params' => [
                'email' => $user->email, 
                'link' => Url::to(['user/activate', 'email' => $user->email, 'token' => $user->activate_token], true), 
            ],
            'from' => Yii::$app->params['infoEmail'],
            'to' => $user->email,
            'subject' => 'Активация аккаунта',
        ]);
    }

    /**
     * @param User $user
     */
    public static function notifyUserRegistration(User $user)
    {
        return static::send([
            'view' => ['html' => 'user-registration-html'],
            'params' => [
                'username' => $user->username, 
                'email' => $user->email,
                'createdAt' => $user->created_at,
            ],
            'from' => Yii::$app->params['infoEmail'],
            'to' => Yii::$app->params['adminEmail'],
            'subject' => "Регистрация нового пользователя ({$user->username})",
        ]);
    }

    /**
     * @param User $user
     * @param array $data
     */
    public static function sendUserData(User $user, array $data)
    {
        return static::send([
            'view' => ['html' => 'user-data-html'],
            'params' => [
                'username' => $user->username, 
                'password' => ArrayHelper::getValue($data, 'password'),
            ],
            'from' => Yii::$app->params['infoEmail'],
            'to' => $user->email,
            'subject' => 'Регистрация на сайте',
        ]);
    }

    /**
     * @param User $user
     * @param array $data
     */
    public static function sendUserChangedPassword(User $user, array $data)
    {
        return static::send([
            'view' => ['html' => 'user-changed-password-html'],
            'params' => [
                'username' => $user->username, 
                'password' => ArrayHelper::getValue($data, 'password'),
            ],
            'from' => Yii::$app->params['infoEmail'],
            'to' => $user->email,
            'subject' => 'Изменения данных',
        ]);
    }
}
