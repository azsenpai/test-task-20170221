<?php

namespace app\helpers;

use Yii;
use yii\console\Controller;

class ConsoleRunner extends Controller
{
    const PHP_COMMAND = 'php';
    const YII_COMMAND = '@app/yii';

    /**
     * Running console command
     *
     * @param string $args Argument that will be passed to console application
     * @return int
     */
    public static function exec($args)
    {
        $command = sprintf('%s %s %s', self::PHP_COMMAND, Yii::getAlias(self::YII_COMMAND), $args);

        $output = [];
        $return_var = self::EXIT_CODE_NORMAL;

        exec($command, $output, $return_var);

        return $return_var;
    }
}
